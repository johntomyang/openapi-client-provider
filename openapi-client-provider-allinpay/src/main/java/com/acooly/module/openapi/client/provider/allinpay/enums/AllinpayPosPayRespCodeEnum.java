/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.allinpay.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike
 * Date: 2017-03-30 15:12:06
 */
public enum AllinpayPosPayRespCodeEnum implements Messageable {
    POS_PAY_0000("0000", "处理成功(最终结果为成功)"),
    POS_PAY_1000("1000", "报文内容检查错或者处理错（具体内容见返回错误信息）"),
    POS_PAY_1001("1001", "报文解释错"),
    POS_PAY_1002("1002", "冲正时无此交易"),
    POS_PAY_1999("1999", "本批交易已经全部失败(最终结果)"),
    POS_PAY_0001("0001", "系统处理失败表示最终失败"),
    POS_PAY_0002("0002", "已撤销表示最终失败"),
    POS_PAY_2000("2000", "系统正在对数据处理 中间状态"),
    POS_PAY_2001("2001", "等待商户审核中间状态"),
    POS_PAY_2002("2002", "商户审核不通过 最终失败"),
    POS_PAY_2003("2003", "等待 受理中间状态"),
    POS_PAY_2004("2004", "不通过受理最终失败"),
    POS_PAY_2005("2005", "等待 复核中间状态"),
    POS_PAY_2006("2006", "不通过复核最终失败"),
    POS_PAY_2007("2007", "提交银行处理中间状态"),
    POS_PAY_2008("2008", "实时交易超时(中间状态,需要查询)"),
    POS_PAY_4000("4000", "跨行交易已发送银行(表示最终成功)"),
    POS_PAY_0397("0397", "不支持该银行的交易"),
    POS_PAY_3001("3001", "查开户方原因"),
    POS_PAY_3002("3002", "没收卡"),
    POS_PAY_3003("3003", "不予承兑"),
    POS_PAY_3004("3004", "无效卡号"),
    POS_PAY_3005("3005", "受卡方与安全保密部门联系"),
    POS_PAY_3006("3006", "已挂失卡"),
    POS_PAY_3007("3007", "被窃卡"),
    POS_PAY_3008("3008", "余额不足"),
    POS_PAY_3009("3009", "无此账户"),
    POS_PAY_3010("3010", "过期卡"),
    POS_PAY_3011("3011", "密码错"),
    POS_PAY_3012("3012", "不允许持卡人进行的交易"),
    POS_PAY_3013("3013", "超出提款限额"),
    POS_PAY_3014("3014", "原始金额不正确"),
    POS_PAY_3015("3015", "超出取款次数限制"),
    POS_PAY_3016("3016", "已挂失折"),
    POS_PAY_3017("3017", "账户已冻结"),
    POS_PAY_3018("3018", "已清户"),
    POS_PAY_3019("3019", "原交易已被取消或冲正"),
    POS_PAY_3020("3020", "账户被临时锁定"),
    POS_PAY_3021("3021", "未登折行数超限"),
    POS_PAY_3022("3022", "存折号码有误"),
    POS_PAY_3023("3023", "当日存入的金额当日不能支取"),
    POS_PAY_3024("3024", "日期切换正在处理"),
    POS_PAY_3025("3025", "PIN格式出错"),
    POS_PAY_3026("3026", "发卡方保密子系统失败"),
    POS_PAY_3027("3027", "原始交易不成功"),
    POS_PAY_3028("3028", "系统忙，请稍后再提交"),
    POS_PAY_3029("3029", "交易已被冲正"),
    POS_PAY_3030("3030", "账号错误"),
    POS_PAY_3031("3031", "账号户名不符"),
    POS_PAY_3032("3032", "账号货币不符"),
    POS_PAY_3033("3033", "无此原交易"),
    POS_PAY_3034("3034", "非活期账号"),
    POS_PAY_3035("3035", "找不到原记录"),
    POS_PAY_3036("3036", "货币错误"),
    POS_PAY_3037("3037", "磁卡未生效"),
    POS_PAY_3038("3038", "非通兑户"),
    POS_PAY_3039("3039", "账户已关户"),
    POS_PAY_3040("3040", "金额错误"),
    POS_PAY_3041("3041", "非存折户"),
    POS_PAY_3042("3042", "交易金额小于该储种的最低支取金额"),
    POS_PAY_3043("3043", "未与银行签约"),
    POS_PAY_3044("3044", "超时拒付"),
    POS_PAY_3045("3045", "合同（协议）号在协议库里不存在"),
    POS_PAY_3046("3046", "合同（协议）号还没有生效"),
    POS_PAY_3047("3047", "合同（协议）号已撤销"),
    POS_PAY_3048("3048", "业务已经清算，不能撤销"),
    POS_PAY_3049("3049", "业务已被拒绝，不能撤销"),
    POS_PAY_3050("3050", "业务已撤销"),
    POS_PAY_3051("3051", "重复业务"),
    POS_PAY_3052("3052", "找不到原业务"),
    POS_PAY_3053("3053", "批量回执包未到规定最短回执期限（M日）"),
    POS_PAY_3054("3054", "批量回执包超过规定最长回执期限（N日）"),
    POS_PAY_3055("3055", "当日通兑业务累计金额超过规定金额"),
    POS_PAY_3056("3056", "退票"),
    POS_PAY_3057("3057", "账户状态错误"),
    POS_PAY_3058("3058", "数字签名或证书错"),
    POS_PAY_3059("3059", "通讯失败"),
    POS_PAY_3065("3065", "户名错"),
    POS_PAY_3080("3080", "超过授权协议最大允许金额或笔数"),
    POS_PAY_3999("3999", "交易失败，具体信息见中文(对于不能明确归入上面的情况置为该反馈码)"),
    ;
    private final String code;
    private final String message;

    private AllinpayPosPayRespCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (AllinpayPosPayRespCodeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static AllinpayPosPayRespCodeEnum find(String code) {
        for (AllinpayPosPayRespCodeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<AllinpayPosPayRespCodeEnum> getAll() {
        List<AllinpayPosPayRespCodeEnum> list = new ArrayList<AllinpayPosPayRespCodeEnum>();
        for (AllinpayPosPayRespCodeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (AllinpayPosPayRespCodeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
