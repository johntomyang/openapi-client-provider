package com.acooly.module.openapi.client.provider.allinpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayApiMsgInfo;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayNotify;
import com.acooly.module.openapi.client.provider.allinpay.enums.AllinpayServiceEnum;
import com.acooly.module.openapi.client.provider.allinpay.support.AllinpayAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @Auther: zhike
 * @Date: 2018/8/30 11:26
 * @Description:
 */
@Getter
@Setter
@AllinpayApiMsgInfo(service = AllinpayServiceEnum.POS_PAY_NOTIFY,type = ApiMessageType.Notify)
public class AllinpayPosPayNotify extends AllinpayNotify {

    /**
     * 交易类型
     */
    @Size(max = 8)
    @NotBlank
    @AllinpayAlias(value = "trxcode")
    private String trxCode;

    /**
     * 收银宝APPID
     */
    @Size(max = 8)
    @NotBlank
    @AllinpayAlias(value = "appid")
    private String appid;

    /**
     * 收银宝商户号
     */
    @Size(max = 15)
    @NotBlank
    @AllinpayAlias(value = "cusid")
    private String cusId;

    /**
     * 调用时间戳
     * yyyymmddhhmmss
     */
    @Size(max = 14)
    @NotBlank
    @AllinpayAlias(value = "timestamp")
    private String timeStamp;

    /**
     * 随机字符串
     * 主要用于随机加签
     */
    @Size(max = 32)
    @NotBlank
    @AllinpayAlias(value = "randomstr")
    private String randomStr;

    /**
     * sign校验码
     */
    @Size(max = 32)
    @NotBlank
    @AllinpayAlias(value = "sign")
    private String sign;

    /**
     * 业务流水号
     * 如订单号，保单号，缴费编号等
     */
    @Size(max = 50)
    @NotBlank
    @AllinpayAlias(value = "bizseq")
    private String bizSeq;

    /**
     * 交易结果状态码
     */
    @Size(max = 4)
    @NotBlank
    @AllinpayAlias(value = "trxstatus")
    private String trxStatus;

    /**
     * 交易金额
     * 单位分
     */
    @Size(max = 20)
    @NotBlank
    @AllinpayAlias(value = "amount")
    private String amount;

    /**
     * 交易流水号
     * 收银宝交易流水号
     */
    @Size(max = 18)
    @NotBlank
    @AllinpayAlias(value = "trxid")
    private String trxId;

    /**
     * 原交易流水
     * 原交易流水,冲正撤销交易本字段不为空
     */
    @Size(max = 18)
    @AllinpayAlias(value = "srctrxid")
    private String srctrxId;

    /**
     * 交易请求日期
     * yyyymmdd
     */
    @Size(max = 8)
    @NotBlank
    @AllinpayAlias(value = "trxday")
    private String trxDay;

    /**
     * 交易完成时间
     * yyyymmddhhmmss
     */
    @Size(max = 14)
    @NotBlank
    @AllinpayAlias(value = "paytime")
    private String payTime;

    /**
     * 终端编码
     */
    @Size(max = 8)
    @NotBlank
    @AllinpayAlias(value = "termid")
    private String termId;

    /**
     * 终端批次号
     */
    @Size(max = 16)
    @NotBlank
    @AllinpayAlias(value = "termbatchid")
    private String termbatchId;

    /**
     * 终端流水
     */
    @Size(max = 6)
    @NotBlank
    @AllinpayAlias(value = "traceno")
    private String traceNo;

    /**
     * 业务关联内容
     * 订单查询时商户订单系统返回的自定义内容
     */
    @Size(max = 160)
    @AllinpayAlias(value = "trxreserve")
    private String trxreserve;

    /**
     * 借贷标志
     * 刷卡消费交易必传
     * 00-借记卡
     * 01-存折
     * 02-信用卡
     * 03-准贷记卡
     * 04-预付费卡
     * 05-境外卡
     * 99- 其他
     */
    @Size(max = 2)
    @AllinpayAlias(value = "accttype")
    private String acctType;

    /**
     * 交易帐号
     * 如果是刷卡交易,则是隐藏的卡号,例如621700****4586
     * 微信则是Openid，支付宝则是userid
     */
    @Size(max = 64)
    @AllinpayAlias(value = "acct")
    private String acct;

    /**
     * 终端授权码
     */
    @Size(max = 20)
    @AllinpayAlias(value = "termauthno")
    private String termauthNo;

    /**
     * 终端参考号
     */
    @Size(max = 50)
    @AllinpayAlias(value = "termrefnum")
    private String termrefNum;
}
