/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by zhike
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.baofu.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike Date: 2017-03-30 15:12:06
 */
public enum BaoFuBatchDeductResCodeEnum implements Messageable {
  //成功
  SUCCESS("0000","交易成功"),
  //交易结果未知,需查询
  BF00100("BF00100","系统异常，请联系宝付"),
  BF00112("BF00112","系统繁忙，请稍后再试"),
  BF00113("BF00113","交易结果未知，请稍后查询"),
  BF00115("BF00115","交易处理中，请稍后查询"),
  BF00144("BF00144","该交易有风险,订单处理中"),
  BF00202("BF00202","交易超时，请稍后查询"),
  BF00254("BF00254","交易处理中"),
  //失败无需查询
  BF00101("BF00101","持卡人信息有误"),
  BF00102("BF00102","银行卡已过有效期，请联系发卡行"),
  BF00103("BF00103","账户余额不足"),
  BF00104("BF00104","交易金额超限"),
  BF00107("BF00107","当前银行卡不支持该业务，请联系发卡行"),
  BF00108("BF00108","交易失败，请联系发卡行"),
  BF00109("BF00109","交易金额低于限额"),
  BF00110("BF00110","该卡暂不支持此交易"),
  BF00111("BF00111","交易失败"),
  BF00116("BF00116","该终端号不存在"),
  BF00118("BF00118","报文中密文解析失败"),
  BF00120("BF00120","报文交易要素缺失"),
  BF00121("BF00121","报文交易要素格式错误"),
  BF00122("BF00122","卡号和支付通道不匹配"),
  BF00123("BF00123","商户不存在或状态不正常，请联系宝付"),
  BF00124("BF00124","商户与终端号不匹配"),
  BF00125("BF00125","商户该终端下未开通此类型交易"),
  BF00126("BF00126","该笔订单已存在"),
  BF00127("BF00127","不支持该支付通道的交易"),
  BF00128("BF00128","该笔订单不存在"),
  BF00129("BF00129","密文和明文中参数【%s】不一致,请确认是否被篡改！"),
  BF00135("BF00135","交易金额不正确"),
  BF00136("BF00136","订单创建失败"),
  BF00140("BF00140","该卡已被注销"),
  BF00141("BF00141","该卡已挂失"),
  BF00146("BF00146","订单金额超过单笔限额"),
  BF00147("BF00147","该银行卡不支持此交易"),
  BF00177("BF00177","非法的交易"),
  BF00190("BF00190","商户流水号不能重复"),
  BF00199("BF00199","订单日期格式不正确"),
  BF00232("BF00232","银行卡未开通认证支付"),
  BF00233("BF00233","密码输入次数超限，请联系发卡行"),
  BF00234("BF00234","单日交易金额超限"),
  BF00235("BF00235","单笔交易金额超限"),
  BF00236("BF00236","卡号无效，请确认后输入"),
  BF00237("BF00237","该卡已冻结，请联系发卡行"),
  BF00249("BF00249","订单已过期，请使用新的订单号发起交易"),
  BF00251("BF00251","订单未支付"),
  BF00253("BF00253","交易拒绝"),
  BF00258("BF00258","手机号码校验失败"),
  BF00262("BF00262","交易金额与扣款成功金额不一致，请联系宝付"),
  BF00311("BF00311","卡类型和biz_type值不匹配"),
  BF00312("BF00312","交易金额不匹配"),
  BF00313("BF00313","商户未开通此产品"),
  BF00315("BF00315","手机号码为空，请重新输入"),
  BF00316("BF00316","ip未绑定，请联系宝付"),
  BF00321("BF00321","身份证号不合法"),
  BF00322("BF00322","卡类型和卡号不匹配"),
  BF00323("BF00323","商户未开通交易模版"),
  BF00324("BF00324","批次号不存在"),
  BF00325("BF00325","订单格式不正确,分隔符缺失"),
  BF00326("BF00326","批次明细笔数超限"),
  BF00327("BF00327","同一批次内订单重复"),
  BF00331("BF00331","卡号校验失败"),
  BF00332("BF00332","交易失败，请重新支付"),
  BF00333("BF00333","该卡有风险，发卡行限制交易"),
  BF00341("BF00341","该卡有风险，请持卡人联系银联客服[95516]"),
  BF08701("BF08701","该卡本次可支付***元，请更换其他银行卡！"),
  BF08702("BF08702","该商户本次可支付***元，请更换其他银行卡或咨询商户客服！"),
  BF08703("BF08703","支付金额不能低于最低限额...元！"),
  BF08704("BF08704","单笔金额超限，该银行单笔可支付xxx元！"),
  BF08705("BF08705","风险交易拒绝（此错误码,共映射5个错误信息）"),
  BF00340("BF00340","商户请求批次号重复"),
  ;

  private final String code;
  private final String message;

  private BaoFuBatchDeductResCodeEnum(String code, String message) {
    this.code = code;
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public String code() {
    return code;
  }

  public String message() {
    return message;
  }

  public static Map<String, String> mapping() {
    Map<String, String> map = new LinkedHashMap<String, String>();
    for (BaoFuBatchDeductResCodeEnum type : values()) {
      map.put(type.getCode(), type.getMessage());
    }
    return map;
  }

  /**
   * 通过枚举值码查找枚举值。
   *
   * @param code 查找枚举值的枚举值码。
   * @return 枚举值码对应的枚举值。
   * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
   */
  public static BaoFuBatchDeductResCodeEnum find(String code) {
    for (BaoFuBatchDeductResCodeEnum status : values()) {
      if (status.getCode().equals(code)) {
        return status;
      }
    }
    return null;
  }

  /**
   * 获取全部枚举值。
   *
   * @return 全部枚举值。
   */
  public static List<BaoFuBatchDeductResCodeEnum> getAll() {
    List<BaoFuBatchDeductResCodeEnum> list = new ArrayList<BaoFuBatchDeductResCodeEnum>();
    for (BaoFuBatchDeductResCodeEnum status : values()) {
      list.add(status);
    }
    return list;
  }

  /**
   * 获取全部枚举值码。
   *
   * @return 全部枚举值码。
   */
  public static List<String> getAllCode() {
    List<String> list = new ArrayList<String>();
    for (BaoFuBatchDeductResCodeEnum status : values()) {
      list.add(status.code());
    }
    return list;
  }
}
