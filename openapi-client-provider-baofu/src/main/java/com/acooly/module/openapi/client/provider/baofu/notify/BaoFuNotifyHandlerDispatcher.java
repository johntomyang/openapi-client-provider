/**
 * coding by zhike
 */
package com.acooly.module.openapi.client.provider.baofu.notify;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.api.notify.AbstractSpringNotifyHandlerDispatcher;
import com.acooly.module.openapi.client.provider.baofu.BaoFuApiServiceClient;
import com.acooly.module.openapi.client.provider.baofu.OpenAPIClientBaoFuProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 富友 支付网关异步通知分发器
 *
 * @author liuyuxiang
 * @date 2016年5月12日
 */
@Component
public class BaoFuNotifyHandlerDispatcher extends AbstractSpringNotifyHandlerDispatcher {

    @Autowired
    private OpenAPIClientBaoFuProperties openAPIClientBaoFuProperties;

    @Resource(name = "baofuApiServiceClient")
    private BaoFuApiServiceClient apiServiceClient;

    @Override
    protected String getServiceKey(String notifyUrl, Map<String, String> notifyData) {
        return Strings.substringAfterLast(notifyUrl, openAPIClientBaoFuProperties.getNotifyUrlPrefix());
    }

    @Override
    protected ApiServiceClient getApiServiceClient() {
        return apiServiceClient;
    }

    public static void main(String[] args) {
        String ss =  Strings.substringAfterLast("http://218.70.106.250:9081/gateway/notify/sdbNotify/netBankNotify", "/");
        System.out.println(ss);
    }
}
