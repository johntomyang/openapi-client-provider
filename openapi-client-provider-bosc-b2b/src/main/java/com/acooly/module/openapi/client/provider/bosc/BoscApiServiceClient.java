package com.acooly.module.openapi.client.provider.bosc;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acooly.core.utils.net.HttpResult;
import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscNotifyDomain;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequestDomain;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponseDomain;
import com.acooly.module.openapi.client.provider.bosc.marshall.BoscRequestMarshall;
import com.acooly.module.openapi.client.provider.bosc.marshall.BoscResponseUnmarshall;
import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BoscApiServiceClient
		extends AbstractApiServiceClient<BoscRequestDomain, BoscResponseDomain, BoscNotifyDomain, BoscResponseDomain> {

	@Autowired
	private BoscRequestMarshall boscRequestMarshall;

	@Autowired
	private BoscResponseUnmarshall boscResponseUnmarshall;

	@Resource(name = "boscb2bHttpTransport")
	private Transport transport;

	@Autowired
	private BoscProperties boscProperties;

	@Autowired
	private BoscMessageFactory boscMessageFactory;

	// 非常不建议封装方式，换人后维护成本高。这里一般采用模板化结构，除特殊情况，不应在这里进行签名
	// 建议处理方式：在BoscRequestMarshall的泛型的第一个参数定义组织的返回结构体（包含报文和header），然后在这里只是发送。
	// 请尽快调整。 review by zhangpu 2017-11-13
	@Override
	public BoscResponseDomain execute(BoscRequestDomain request) {
		log.info("上海银行（B2B）入参信息：" + JSONObject.toJSONString(request));
		BoscResponseDomain domain = (BoscResponseDomain) boscMessageFactory.getResponse(request.getService());
		try {
			beforeExecute(request);

			String[] signInfo = boscRequestMarshall.marshal(request);

			log.info("上海银行（B2B）请求报文：" + signInfo[0]);
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("channels", "BXKJ");
			headers.put("signData", signInfo[1]);
			headers.put("signCert", signInfo[2]);
			String url = boscProperties.getRequestUrl() + "/" + request.getChannelUrl();
			HttpResult result = getTransport().request(signInfo[0], url, headers);
			log.info("上海银行（B2B）响应报文：" + result);

			domain = boscResponseUnmarshall.unmarshal(result, request.getService());

			afterExecute(domain);
		} catch (Exception e) {
			log.error("上海银行（B2B）服务异常：" + e.getMessage());
			domain.setResultDesc(e.getMessage());
		}
		return domain;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ApiMarshal<String, BoscRequestDomain> getRequestMarshal() {
		return null;
	}

	@Override
	protected ApiMarshal<String, BoscRequestDomain> getRedirectMarshal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ApiUnmarshal<BoscNotifyDomain, Map<String, String>> getNoticeUnmarshal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ApiUnmarshal<BoscResponseDomain, Map<String, String>> getReturnUnmarshal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Transport getTransport() {
		return this.transport;
	}

	@Override
	protected ApiUnmarshal<BoscResponseDomain, String> getResponseUnmarshal() {
		// TODO Auto-generated method stub
		return null;
	}

}
