package com.acooly.module.openapi.client.provider.bosc.message.request;

import org.hibernate.validator.constraints.NotBlank;

import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequestDomain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActivateUpBindRequest extends BoscRequestDomain {

	@NotBlank
	private String operatorId;

}
