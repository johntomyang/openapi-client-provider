package com.acooly.module.openapi.client.provider.bosc.message.fund.info;

import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscCheckFileTypeEnum;

import java.io.Serializable;

/**
 * Created by liubin@prosysoft.com on 2017/10/12.
 */
public class ConfirmCheckInfo implements Serializable {
	
	/**
	 * 对账确认文件类型
	 */
	private BoscCheckFileTypeEnum fileType;
	
	
	public ConfirmCheckInfo () {
	}
	
	public ConfirmCheckInfo (BoscCheckFileTypeEnum fileType) {
		this.fileType = fileType;
	}
	
	public BoscCheckFileTypeEnum getFileType () {
		return fileType;
	}
	
	public void setFileType (BoscCheckFileTypeEnum fileType) {
		this.fileType = fileType;
	}
}
