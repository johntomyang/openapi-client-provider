package com.acooly.module.openapi.client.provider.fbank;

import com.acooly.module.openapi.client.api.notify.ApiServiceClientServlet;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.fbank.notify.FbankApiServiceClientServlet;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;

import static com.acooly.module.openapi.client.provider.fbank.OpenAPIClientFbankProperties.PREFIX;


@EnableConfigurationProperties({OpenAPIClientFbankProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class OpenAPIClientFbankConfigration {

    @Autowired
    private OpenAPIClientFbankProperties openAPIClientFbankProperties;

    @Bean("fbankHttpTransport")
    public Transport FbankHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setConnTimeout(String.valueOf(openAPIClientFbankProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientFbankProperties.getReadTimeout()));
        return httpTransport;
    }

    /**
     * 易行通SDK-servlet注册
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean getApiSDKServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        FbankApiServiceClientServlet apiServiceClientServlet = new FbankApiServiceClientServlet();
        bean.setServlet(apiServiceClientServlet);
        bean.addInitParameter(ApiServiceClientServlet.NOTIFY_DISPATCHER_BEAN_NAME_KEY, "fbankNotifyHandlerDispatcher");
        List<String> urlMappings = Lists.newArrayList();       //网关异步通知地址
        urlMappings.add("/gateway/notify/fbankNotify/*");//访问，可以添加多个
        bean.setUrlMappings(urlMappings);
        bean.setLoadOnStartup(3);
        return bean;
    }
}
