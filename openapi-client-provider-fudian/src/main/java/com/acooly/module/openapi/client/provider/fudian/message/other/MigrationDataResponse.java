/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 01:57:25 创建
 */
package com.acooly.module.openapi.client.provider.fudian.message.other;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianResponse;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhangpu 2018-02-22 01:57:25
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.MIGRATION_DATA, type = ApiMessageType.Response)
public class MigrationDataResponse extends FudianResponse {

}