/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:32:16 创建
 */
package com.acooly.module.openapi.client.provider.fudian.message.query.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:32:16
 */
@Getter
@Setter
public class LoanAccountLog {

    /**
     * 操作金额
     * 当前发生的交易金额
     */
    @NotEmpty
    @Length(max = 20)
    private String amount;

    /**
     * 可用余额
     * 可用余额
     */
    @NotEmpty
    @Length(max = 20)
    private String balance;

    /**
     * 发生时间
     * 添加记录时间
     */
    @NotEmpty
    @Length(max = 32)
    private String createTime;

    /**
     * 冻结金额
     * 冻结金额
     */
    @NotEmpty
    @Length(max = 20)
    private String freezeBalance;

    /**
     * 标的账户编号
     * 存管系统为标的生成的账户的编号
     */
    @NotEmpty
    @Length(max = 32)
    private String loanAccNo;

    /**
     * 标的名称
     * 标的名称
     */
    @NotEmpty
    @Length(max = 256)
    private String loanName;

    /**
     * 标的唯一标识
     * 存管系统为标的生成的唯一性标识
     */
    @NotEmpty
    @Length(max = 32)
    private String loanTxNo;

    /**
     * 商户名称
     * 商户名称
     */
    @NotEmpty
    @Length(max = 32)
    private String merchantName;

    /**
     * 托管用户id
     * 商户号
     */
    @NotEmpty
    @Length(max = 32)
    private String merchantNo;

    /**
     * 交易说明
     * 交易说明信息
     */
    @NotEmpty
    @Length(max = 256)
    private String remark;

    /**
     * 交易方
     * 交易方存管用户名
     */
    @NotEmpty
    @Length(max = 32)
    private String toUserName;
}