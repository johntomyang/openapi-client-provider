/**
 * create by zhangpu
 * date:2015年3月11日
 */
package com.acooly.module.openapi.client.provider.fuyou.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhangpu
 * 
 */
@Getter
@Setter
public class FuyouNotify extends FuyouResponse {
    /**
     * 验签密钥
     */
    private String key;
}
