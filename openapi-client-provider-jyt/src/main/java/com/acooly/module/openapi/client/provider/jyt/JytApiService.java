/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.jyt;

import com.acooly.module.openapi.client.provider.jyt.domain.JytNotify;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.message.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhangpu
 */
@Service
public class JytApiService {

    @Resource(name = "jytApiServiceClient")
    private JytApiServiceClient apiServiceClient;

    @Autowired
    private OpenAPIClientJytProperties openAPIClientJytProperties;


    /**
     * 代扣申请
     * @param request
     * @return
     */
    public JytDeductApplyResponse deductApply(JytDeductApplyRequest request) {
        request.setService(JytServiceEnum.DEDUCT_APPLY.getCode());
        return (JytDeductApplyResponse)apiServiceClient.execute(request);
    }

    /**
     * 代扣充值
     * @param request
     * @return
     */
    public JytDeductPayResponse deductPay(JytDeductPayRequest request) {
        request.setService(JytServiceEnum.DEDUCT_PAY.getCode());
        return (JytDeductPayResponse)apiServiceClient.execute(request);
    }

    /**
     * 重新获取验证码
     * @param request
     * @return
     */
    public JytRetrievesSmsCodeResponse retrievesSmsCode(JytRetrievesSmsCodeRequest request) {
        request.setService(JytServiceEnum.RETRIEVES_SMS_CODE.getCode());
        return (JytRetrievesSmsCodeResponse)apiServiceClient.execute(request);
    }

    /**
     * 支付交易单笔查询
     * @param request
     * @return
     */
    public JytTradeOrderQueryResponse tradeOrderQuery(JytTradeOrderQueryRequest request) {
        request.setService(JytServiceEnum.TRADE_ORDER_QUERY.getCode());
        return (JytTradeOrderQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 四要素验卡发送短息
     * @param request
     * @return
     */
    public JytCheckCardApplyResponse checkCardApply(JytCheckCardApplyRequest request) {
        request.setService(JytServiceEnum.CHECK_CARD_APPLY.getCode());
        return (JytCheckCardApplyResponse)apiServiceClient.execute(request);
    }

    /**
     * 四要素验卡确认
     * @param request
     * @return
     */
    public JytCheckCardConfirmResponse checkCardConfirm(JytCheckCardConfirmRequest request) {
        request.setService(JytServiceEnum.CHECK_CARD_CONFIRM.getCode());
        return (JytCheckCardConfirmResponse)apiServiceClient.execute(request);
    }


    /**
     * 解除实名支付银行卡
     * @param request
     * @return
     */
    public JytUnBindCardResponse unBindCard(JytUnBindCardRequest request) {
        request.setService(JytServiceEnum.UN_BIND_CARD.getCode());
        return (JytUnBindCardResponse)apiServiceClient.execute(request);
    }

    /**
     * 解析异步通知
     *
     * @param request
     * @param serviceKey
     * @return
     */
    public JytNotify notice(HttpServletRequest request, String serviceKey) {
        return  apiServiceClient.notice(request,serviceKey);
    }

}
