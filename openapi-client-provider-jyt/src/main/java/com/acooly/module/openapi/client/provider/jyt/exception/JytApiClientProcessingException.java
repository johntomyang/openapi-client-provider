package com.acooly.module.openapi.client.provider.jyt.exception;

import com.acooly.module.openapi.client.api.exception.ApiClientNetworkException;

/**
 * @author zhike 2018/2/2 10:39
 * 响应处理异常，业务端接收到此异常，应该订单应该当成处理中来处理，需要通过查询接口或者异步通知来订正
 * 订单最终状态，不应该直接当成失败来处理
 */
public class JytApiClientProcessingException extends ApiClientNetworkException {
    public JytApiClientProcessingException() {
        super();
    }

    public JytApiClientProcessingException(String message, Throwable cause) {
        super (message, cause);
    }

    public JytApiClientProcessingException(String message) {
        super (message);
    }

    public JytApiClientProcessingException(Throwable cause) {
        super (cause);
    }
}
