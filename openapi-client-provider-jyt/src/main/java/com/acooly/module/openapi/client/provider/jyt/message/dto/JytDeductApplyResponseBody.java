package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/5/8 0:11
 */
@Getter
@Setter
@XStreamAlias("body")
public class JytDeductApplyResponseBody implements Serializable {

    /**
     * 验证状态
     ①当报文头的响应码为S0000000时有值，01平台处理中（短信发送成功）
     ②当报文头的响应码为其它时，无报文体并表示下单失败
     */
    @XStreamAlias("tran_state")
    private String tranState;

    /**
     * 备注信息
     * 验证结果说明
     */
    @XStreamAlias("remark")
    private String remark;
}
