/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.newyl;

/**
 * @author
 */
public class NewYlConstants {
    /**
     * 签名实现KEY和秘钥
     */
    public static final String SIGN = "signature";
    public static final String SIGN_TYPE = "RSA";
    public static final String SIGNER_KEY = "newYl";


    public static final String ORDER_NO_NAME = "out_trade_no";
    public static final String PROVIDER_NAME = "newYlProvider";
    public static final String XML_HEAD = "<?xml version=\"1.0\" encoding=\"GBK\"?>";
}
