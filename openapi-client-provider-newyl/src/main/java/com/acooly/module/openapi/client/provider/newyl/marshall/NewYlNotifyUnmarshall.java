/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.newyl.marshall;


import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.newyl.domain.NewYlNotify;
import com.acooly.module.openapi.client.provider.newyl.partner.NewYlPartnerIdLoadManager;
import com.acooly.module.safety.key.KeyLoadManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import javax.annotation.Resource;

/**
 * @author
 */
@Service
public class NewYlNotifyUnmarshall extends NewYlMarshallSupport
        implements ApiUnmarshal<NewYlNotify, Map<String, String>> {
    private static final Logger logger = LoggerFactory.getLogger(NewYlNotifyUnmarshall.class);
    @Resource(name = "newYlMessageFactory")
    private MessageFactory messageFactory;

    @Resource(name = "newYlPartnerIdLoadManager")
    private NewYlPartnerIdLoadManager partnerIdLoadManager;

    @Autowired
    private KeyLoadManager keyStoreLoadManager;

    @SuppressWarnings("unchecked")
    @Override
    public NewYlNotify unmarshal(Map<String, String> message, String serviceName) {
        try {
            logger.info("异步通知{}", message);

            return null;
        } catch (Exception e) {
            throw new ApiClientException("异步通知 解析失败:" + e.getMessage());
        }

    }


}
