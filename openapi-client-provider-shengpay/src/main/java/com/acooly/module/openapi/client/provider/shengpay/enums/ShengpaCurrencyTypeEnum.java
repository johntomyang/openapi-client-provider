/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.shengpay.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by liubin@prosysoft.com on 2017/9/27.
 */
public enum ShengpaCurrencyTypeEnum implements Messageable {

    CURRENCY_TYPE_CNY("CNY","人民币"),
    CURRENCY_TYPE_USD("USD","美元"),
    CURRENCY_TYPE_GBP("GBP","英镑"),
    CURRENCY_TYPE_HKD("HKD","港币"),
    CURRENCY_TYPE_SGD("SGD","新加坡"),
    CURRENCY_TYPE_JPY("JPY","日元"),
    CURRENCY_TYPE_CAD("CAD","加拿大"),
    CURRENCY_TYPE_AUD("AUD","澳元"),
    CURRENCY_TYPE_EUR("EUR","欧元"),
    CURRENCY_TYPE_CHF("CHF","瑞士法郎"),
    ;

    private final String code;
    private final String message;

    private ShengpaCurrencyTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (ShengpaCurrencyTypeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static ShengpaCurrencyTypeEnum find(String code) {
        for (ShengpaCurrencyTypeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("FudianAuditStatusEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<ShengpaCurrencyTypeEnum> getAll() {
        List<ShengpaCurrencyTypeEnum> list = new ArrayList<ShengpaCurrencyTypeEnum>();
        for (ShengpaCurrencyTypeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (ShengpaCurrencyTypeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
