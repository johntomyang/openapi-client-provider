/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-24 16:11 创建
 */
package com.acooly.module.openapi.client.provider.sinapay.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 同步请求报文组装
 *
 * @author zhike 2018-1-23 16:11
 */
@Slf4j
@Component
public class SinapayRequestMarshall extends SinapayAbstractMarshall implements ApiMarshal<String, SinapayRequest> {

    @Override
    public String marshal(SinapayRequest source) {
        return doMarshall(source);
    }
}
