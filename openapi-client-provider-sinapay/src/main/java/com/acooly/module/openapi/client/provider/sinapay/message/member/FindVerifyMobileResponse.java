package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRedirect;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/7/10 15:30
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.FIND_VERIFY_MOBILE, type = ApiMessageType.Response)
public class FindVerifyMobileResponse extends SinapayRedirect {

}
