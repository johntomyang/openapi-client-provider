package com.acooly.module.openapi.client.provider.weixin;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.acooly.core.utils.Strings;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhangpu@acooly.cn
 */
@ConfigurationProperties(prefix = OpenAPIClientWeixinProperties.PREFIX)
@Getter
@Setter
public class OpenAPIClientWeixinProperties {

    public static final String PREFIX = "acooly.openapi.client.weixin";

    /**
     * 是否启用
     */
    private boolean enable = true;
    /**
     * 微信网关地址
     */
    private String gatewayUrl;
    /**
     * appid
     */
    private String appId;
    
    /**
     * 商户id
     */
    private String mchId;
    /**
     * 证书地址
     */
    private String keyPath;
    /**
     * 证书key
     */
    private String mchKey;
    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 报文中签名字段名称
     */
    private String sign;
    /**
     * 签名方式
     */
    private String signType;
    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;
    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;
    /**
     * 异步通知URL前缀
     */
    private String notifyUrlPrefix = "/";

    private String notifyUrl = "/gateway/notify/weixinNotify";

    protected String getCanonicalUrl(String prefix, String postfix) {
        if (Strings.endsWith(prefix, "/")) {
            prefix = Strings.removeEnd(prefix, "/");
        }
        if (Strings.startsWith(postfix, "/")) {
            return prefix + postfix;
        } else {
            return prefix + "/" + postfix;
        }
    }
}
