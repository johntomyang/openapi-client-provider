/** create by zhangpu date:2015年3月11日 */
package com.acooly.module.openapi.client.provider.weixin.domain;

import com.acooly.module.openapi.client.provider.weixin.support.WeixinAlias;

import lombok.Getter;
import lombok.Setter;

/** @author zhangpu */
@Getter
@Setter
public class WeixinNotify extends WeixinResponse {
	
	/** 商户订单号 */
	@WeixinAlias(value = "out_trade_no")
	private String outTradeNo;
	/** 微信支付订单号 */
	@WeixinAlias(value = "transaction_id")
	private String transactionId;
}
