/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.wewallet;

import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletNotify;
import com.acooly.module.openapi.client.provider.wewallet.enums.WeWalletServiceEnum;
import com.acooly.module.openapi.client.provider.wewallet.message.WeWalletSmsRequest;
import com.acooly.module.openapi.client.provider.wewallet.message.WeWalletSmsResponse;
import com.acooly.module.openapi.client.provider.wewallet.message.WeWalletTicketRequest;
import com.acooly.module.openapi.client.provider.wewallet.message.WeWalletTicketResponse;
import com.acooly.module.openapi.client.provider.wewallet.message.WeWalletTokenRequest;
import com.acooly.module.openapi.client.provider.wewallet.message.WeWalletTokenResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhangpu 2017-09-25 14:40
 */
@Service
public class WeWalletApiService {
	
	@Resource(name = "weWalletApiServiceClient")
	private WeWalletApiServiceClient weWalletApiServiceClient;

	@Autowired
	private WeWalletProperties weWalletProperties;

	/**
	 * 手机短信验证
	 * @param request
	 * @return
	 */
	public WeWalletSmsResponse weWalletSms(WeWalletSmsRequest request) {
		request.setBizType(WeWalletServiceEnum.WEBANK_SMS.code());
		return (WeWalletSmsResponse) weWalletApiServiceClient.execute(request);
	}


	/**
	 * 解析异步通知
	 *
	 * @param request
	 * @param serviceKey
	 * @return
	 */
	public WeWalletNotify notice(HttpServletRequest request, String serviceKey) {
		return  weWalletApiServiceClient.notice(request,serviceKey);
	}


	/**
	 * 获取访问 令牌 接口
	 * @param request
	 * @return
	 */
	public WeWalletTokenResponse weWalletToken(WeWalletTokenRequest request) {
		request.setBizType(WeWalletServiceEnum.WEWALLET_ACCESS_TOKEN.code());
		return (WeWalletTokenResponse) weWalletApiServiceClient.execute(request);
	}

	/**
	 * 获取 API票据 接口
	 * @param request
	 * @return
	 */
	public WeWalletTicketResponse weWalletTicket(WeWalletTicketRequest request) {
		request.setBizType(WeWalletServiceEnum.WEWALLET_TICKET.code());
		return (WeWalletTicketResponse) weWalletApiServiceClient.execute(request);
	}

}
