package com.acooly.module.openapi.client.provider.wewallet.marshall;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletRequest;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class WeWalletRedirectPostMarshall extends WeWalletMarshallSupport implements ApiMarshal<PostRedirect, WeWalletRequest> {

    @Override
    public PostRedirect marshal(WeWalletRequest source) {
        PostRedirect postRedirect = new PostRedirect();
        if(Strings.isBlank(source.getGatewayUrl())) {
            source.setGatewayUrl(getProperties().getGatewayUrl());
        }
        if(Strings.isBlank(source.getPartnerId())) {
            source.setPartnerId(getProperties().getPartnerId());
        }
        postRedirect.setRedirectUrl(source.getGatewayUrl());
        //postRedirect.setFormDatas(doMarshall(source));
        log.info("跳转报文: {}", postRedirect);
        return postRedirect;
    }

}
