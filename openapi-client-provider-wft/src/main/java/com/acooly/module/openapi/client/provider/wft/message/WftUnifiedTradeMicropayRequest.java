package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftRequest;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/** @author zhike 2017/11/27 14:16 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.UNIFIED_TRADE_MICROPAY, type = ApiMessageType.Request)
public class WftUnifiedTradeMicropayRequest extends WftRequest {

  /** 订单号 */
  @WftAlias(value = "out_trade_no")
  @NotBlank(message = "订单号不能为空")
  @Size(max = 32)
  private String outTradeNo;

  /** 设备号 */
  @WftAlias(value = "device_info")
  @Size(max = 32)
  private String deviceInfo;

  /** 商品描述 */
  @WftAlias(value = "body")
  @Size(max = 127)
  @NotBlank(message = "商品描述不能为空")
  private String body;

  /**
   * 单品信息
   * 单品优惠活动该字段必传，且必须按照规范上传，JSON格式，详见https://open.swiftpass.cn/openapi/doc?index_1=2&index_2=1&chapter_1=1016&chapter_2=297
   */
  @WftAlias(value = "goods_detail")
  @Size(max = 1024)
  private String goodsDetail;

  /** 附加信息 */
  @WftAlias(value = "attach")
  private String attach;
  /** 总金额，以分为单位，不允许包含任何字、符号 */
  @WftAlias(value = "total_fee")
  @NotBlank(message = "金额不能为空")
  private String amount;

  /** 终端IP，订单生成的机器 IP */
  @WftAlias(value = "mch_create_ip")
  @NotBlank(message = "终端IP不能为空")
  @Size(max = 16)
  private String mchCreateIp;

  /** 授权码 扫码支付授权码， 设备读取用户展示的条码或者二维码信息 */
  @WftAlias(value = "auth_code")
  @NotBlank(message = "授权码不能为空")
  @Size(max = 128)
  private String authCode;

  /**
   * 订单生成时间 订单生成时间，格式为yyyymmddhhmmss，如2009年12月25日9点10分10秒表示为20091225091010。时区为GMT+8
   * beijing。该时间取自商户服务器。注：订单生成时间与超时时间需要同时传入才会生效。
   */
  @WftAlias(value = "time_start")
  @Size(max = 14)
  private String timeStart;

  /**
   * 订单超时时间 订单失效时间，格式为yyyymmddhhmmss，如2009年12月27日9点10分10秒表示为20091227091010。时区为GMT+8
   * beijing。该时间取自商户服务器。注：订单生成时间与超时时间需要同时传入才会生效。
   */
  @WftAlias(value = "time_expire")
  @Size(max = 14)
  private String timeExpire;

  /** 操作员 操作员帐号,默认为商户号 */
  @WftAlias(value = "op_user_id")
  @Size(max = 32)
  private String opUserId;

  /** 门店编号 */
  @WftAlias(value = "op_shop_id")
  @Size(max = 32)
  private String opShopId;

  /** 设备编号 */
  @WftAlias(value = "op_device_id")
  @Size(max = 32)
  private String opDeviceId;

  /** 商品标记 */
  @WftAlias(value = "goods_tag")
  @Size(max = 32)
  private String goodsTag;

  /** 随机字符串 */
  @WftAlias(value = "nonce_str")
  @Size(max = 32)
  private String nonceStr = Ids.oid();
}
