package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftResponse;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @author zhike 2017/12/7 10:26
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.UNIFIED_TRADE_REFUNDQUERY, type = ApiMessageType.Response)
public class WftUnifiedTradeRefundQueryResponse extends WftResponse {

    /**
     * --------------------------以下字段在 status 和 result_code 都为 0的时候有返回--------------------------------
     */
    /**
     * 商户订单号
     */
    @WftAlias(value = "out_trade_no")
    private String outTradeNo;

    /**
     * 平台订单号
     */
    @WftAlias(value = "transaction_id")
    private String transactionId;

    /**
     * 退款记录数
     */
    @WftAlias(value = "refund_count")
    private String refundCount;

    /**
     * 商户退款单号
     */
    @WftAlias(value = "out_refund_no_0")
    private String outRefundNo;

    /**
     * 平台退款单号
     */
    @WftAlias(value = "refund_id_0")
    private String refundId;

    /**
     * 退款渠道
     */
    @WftAlias(value = "refund_channel_0")
    private String refundChannel;

    /**
     * 退款金额
     */
    @WftAlias(value = "refund_fee_0")
    private String refundFee;

    /**
     * 现金券退款金额
     */
    @WftAlias(value = "coupon_refund_fee_0")
    private String couponRefundFee;

    /**
     * 退款时间 yyyyMMddHHmmss
     */
    @WftAlias(value = "refund_time_0")
    private String refundTime;

    /**
     * 退款状态
     * 退款状态：SUCCESS—退款成功
     * FAIL—退款失败
     * PROCESSING—退款处理中
     * NOTSURE—未确定， 需要商户原退款单号重新发起
     * CHANGE—转入代发，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，资金回流到商户的现金帐号，需要商户人工干预，通过线下或者平台转账的方式进行退款。
     */
    @WftAlias(value = "refund_status_0")
    private String refundStatus;


}
