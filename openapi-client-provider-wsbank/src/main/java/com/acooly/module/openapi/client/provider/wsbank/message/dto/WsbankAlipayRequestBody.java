package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author sunjx 2018/5/23 14:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankAlipayRequestBody implements Serializable {

	private static final long serialVersionUID = -4957566653955247068L;

	/**
	 * 支付类型
	 */
	@Size(max = 32)
	@XStreamAlias("PayType")
	@NotBlank
	private String payType;

	/**
	 * 清算方式
	 */
	@Size(max = 32)
	@XStreamAlias("SettleType")
	@NotBlank
	private String settleType = "T1";
	
	/**
	 * HTTP/HTTPS开头字符串
	 */
	@Size(max = 256)
	@XStreamAlias("ReturnUrl")
	private String returnUrl;
	
	/**
	 * 对一笔交易的具体描述信息
	 */
	@Size(max = 128)
	@XStreamAlias("Body")
	@NotBlank
	private String body;
	
	/**
	 * 商品的标题/交易标题/订单标题/订单关键字等
	 */
	@Size(max = 256)
	@XStreamAlias("Subject")
	@NotBlank
	private String subject;
	
	/**
	 * 商户网站唯一订单号
	 */
	@Size(max = 64)
	@XStreamAlias("OutTradeNo")
	@NotBlank
	private String outTradeNo;
	
	/**
	 * 该笔订单允许的最晚付款时间，逾期将关闭交易
	 */
	@Size(max = 6)
	@XStreamAlias("ExpireExpress")
	private String expireExpress;
	
	/**
	 * 币种
	 */
	@Size(max = 3)
	@XStreamAlias("Currency")
	@NotBlank
	private String currency = "CNY";
	
	/**
	 * 订单总金额单位为分
	 */
	@XStreamAlias("TotalAmount")
	@NotBlank
	private String totalAmount;
	
	
	/**
	 * 针对用户授权接口，获取用户相关数据时，用于标识用户授权关系
	 */
	@Size(max = 40)
	@XStreamAlias("AuthToken")
	private String authToken;
	
	
	/**
	 * 商品主类型：0—虚拟类商品，1—实物类商品注：虚拟类商品不支持使用花呗渠道
	 */
	@Size(max = 2)
	@XStreamAlias("GoodsType")
	private String goodsType;
	
	/**
	 * 公用回传参数
	 */
	@Size(max = 128)
	@XStreamAlias("Attach")
	private String attach;
	
	
	
	/**
	 * 优惠参数注
	 */
	@Size(max = 521)
	@XStreamAlias("PromoParams")
	private String promoParams;
	
	/**
	 * 禁用渠道，用户不可用指定渠道支付当有多个渠道时用“,”分隔注：与enable_pay_channels互斥
	 */
	@Size(max = 128)
	@XStreamAlias("PayLimit")
	private String payLimit;
	
	/**
	 * 商户门店编号
	 */
	@Size(max = 32)
	@XStreamAlias("StoreId")
	private String storeId;
	
	/**
	 * 添加该参数后在h5支付收银台会出现返回按钮，可用于用户付款中途退出并返回到该参数指定的商户网站地址
	 */
	@Size(max = 400)
	@XStreamAlias("QuitUrl")
	private String quitUrl;
	
	/**
	 * 商户号
	 */
	@Size(max = 64)
	@XStreamAlias("MerchantId")
	@NotBlank
	private String merchantId;
	
	/**
	 * 合作方机构号
	 */
	@Size(max = 64)
	@XStreamAlias("IsvOrgId")
	@NotBlank
	private String isvOrgId;
	
	/**
	 * 支持系统商返佣，该参数作为系统商返佣数据提取的依据
	 */
	@Size(max = 64)
	@XStreamAlias("SysServiceProviderId")
	private String SysServiceProviderId;
	
	/**
	 * 花呗交易分期数，可选值：3：3期  6：6期  12：12期   每期间隔为一个月。例如，选择3期，所垫付的资金及利息按3个月等额本息还款，每月还款一笔。
	 */
	@XStreamAlias("CheckLaterNm")
	private String checkLaterNm;
	
	/**
	 * 卖家承担收费比例，商家承担手续费传入100，用户承担手续费传入0，仅支持传入100、0两种，其他比例暂不支持
	 */
	@XStreamAlias("CheckLaterSellerPercent")
	private String checkLaterSellerPercent;
}
