package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.SubMerchantDetail;
import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankCommonMerchantClearCard;
import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankCommonMerchantFeeRate;
import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankSiteInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * @author weili 2018/5/22 16:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankMerchantUpdateRequestBody implements Serializable {
    /**
     *合作方机构号（网商银行分配）
     */
    @Size(max = 32)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    /**
     * 商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。
     */
    @Size(max = 32)
    @NotBlank
    @XStreamAlias("MerchantId")
    private String merchantId;
    

	/**
	 * 商户经营类型。可选值： 01:实体特约商户 02:网络特约商户 03:实体兼网络特约商户
	 */
    @Size(max = 8)
	@XStreamAlias("DealType")
	private String dealType="01";

	/**
	 * 商户清算资金是否支持T+0到账。可选值： Y：支持 N：不支持
	 */
    @Size(max = 8)
	@XStreamAlias("SupportPrepayment")
	private String supportPrepayment ="N";
	
	/**
	 * 结算方式。商户清算资金结算方式，可选值： 01：结算到他行卡 02：结算到余利宝 03：结算到活期户（暂不开放） 04：自提打款（暂不开放）
	 */
    @Size(max = 8)
	@XStreamAlias("SettleMode")
	private String settleMode = "05";

	/**
	 * 经营类目。参见附录的经营类目上送。
	 */
    @Size(max = 16)
    @NotBlank
	@XStreamAlias("Mcc")
	private String mcc="2016062900190337";
	
	/**
	 * 支付宝线上经营类目。参见附录的支付宝线上经营类目。
	 */
    @Size(max =4)
    @NotBlank
	@XStreamAlias("OnlineMcc")
	private String onlineMcc="5411";

	
	/**
	 * 商户详情，json格式base64编码，具体报文定义参考下面的商户详情
	 */
	@XStreamAlias("MerchantDetail")
	private String merchantDetail;

	/**
	 * 支付宝线上站点信息
	 */
//	@NotBlank
	@XStreamAlias("SiteInfo")
	private String siteInfo;


	/**
	 * 手续费列表
	 */
	@XStreamAlias("FeeParamList")
	private String feeParamList;

	/**
	 * 清算卡参数。json格式base64编码，具体报文定义参考下面的清算卡，仅结算方式为“01结算到他行卡”需要填写
	 */
	@XStreamAlias("BankCardParam")
	private String bankCardParam;

	/**
	 * 外部交易号。合作方系统生成的外部交易号，同一交易号被视为同一笔交易。
	 */
	@Size(max = 64)
	@NotBlank
	@XStreamAlias("OutTradeNo")
	private String outTradeNo;
	
	
	@XStreamOmitField
	private SubMerchantDetail merchantDetailObj;
	
	@XStreamOmitField
	private List<WsbankCommonMerchantFeeRate> FeeParamListObj;
	
	@XStreamOmitField
	private WsbankCommonMerchantClearCard merchantClearCardObj;

	@XStreamOmitField
	private List<WsbankSiteInfo> wsbankSiteInfoObj;
	
}


