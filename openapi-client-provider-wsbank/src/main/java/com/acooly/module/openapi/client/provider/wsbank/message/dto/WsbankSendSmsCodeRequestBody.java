package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/5/23 9:56
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankSendSmsCodeRequestBody implements Serializable {
    /**
     *合作方机构号（网商银行分配）
     */
    @Size(max = 64)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    /** 短信业务类型。本短信验证码对应的业务。可选值： 01：开通银行账户 02：换绑银行卡 03：更换银行预留手机号 04：商户入驻申请 05：余利宝提现 */
    @Size(max = 64)
    @XStreamAlias("BizType")
    @NotBlank
    private String bizType;

    /**
     * 手机号。短信验证码接受手机号。当BizType为01、03、04时必填，其余情况不可填。
     */
    @Size(max = 64)
    @XStreamAlias("Mobile")
    private String mobileNo;
    /**
     *商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。当BizType为02、03、05时必填，其余情况不可填
     */
    @Size(max = 64)
    @XStreamAlias("MerchantId")
    private String merchantId;

    /**
     *外部交易号。支付交易合作方系统提交的交易号。
     */
    @Size(max = 64)
    @XStreamAlias("OutTradeNo")
    @NotBlank
    private String outTradeNo;
}
