package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author sunjx
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankWithdrawConfirmResponseBody implements Serializable {

	private static final long serialVersionUID = -291343913193044990L;

	/**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;

    /**
     * 外部交易号
     */
    @XStreamAlias("OutTradeNo")
    private String outTradeNo;

    /**
     * 网商支付订单号
     */
    @XStreamAlias("OrderNo")
    private String orderNo;
    
    /**
     * 订单金额
     */
    @XStreamAlias("TotalAmount")
    private String totalAmount;

    /**
     *外部交易号
     */
    @XStreamAlias("Currency")
    private String currency;
}
