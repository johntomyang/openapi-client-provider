package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/5/24 14:51
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankWithdrawNotifyBody implements Serializable {

    /**
     * 商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。
     */
    @XStreamAlias("MerchantId")
    private String merchantId;

    /**
     * 合作方机构号（网商银行分配）
     */
    @XStreamAlias("IsvOrgId")
    private String isvOrgId;

    /**
     * 原外部订单请求流水号
     */
    @XStreamAlias("OutTradeNo")
    private String outTradeNo;

    /**
     * 原网商订单号
     */
    @XStreamAlias("OrderNo")
    private String orderNo;

    /**
     * 订单金额(金额为分)
     */
    @XStreamAlias("TotalAmount")
    private String totalAmount;

    /**
     * 币种，默认CNY
     */
    @XStreamAlias("Currency")
    private String currency;

    /**
     * 平台设置提现手续用（银行控制盖帽比率），用户实收资金= TotalAmount - PlatformFee
     */
    @XStreamAlias("PlatformFee")
    private String platformFee;

    /**
     * 币种，默认CNY
     */
    @XStreamAlias("FeeCurrency")
    private String feeCurrency;

    /** 出款渠道id （9001大额、9002小额、9003超网、9100支付宝） */
    @XStreamAlias("ChannelId")
    private String channelId;

    /**
     *状态，状态(SUCCESS, DEALING ，FAIL)
     */
    @XStreamAlias("Status")
    private String status;

    /**
     * 银行联行号
     */
    @XStreamAlias("ContactLine")
    private String contactLine;

    /**
     * 绑定银行卡号
     */
    @XStreamAlias("BankCardNo")
    private String bankCardNo;

    /**
     * 绑定卡号户名
     */
    @XStreamAlias("BankCertName")
    private String bankCertName;

    /**
     * 提现时间
     */
    @XStreamAlias("WithdrawApplyDate")
    private String withdrawApplyDate;

    /**
     * 提现完成时间
     */
    @XStreamAlias("WithdrawFinishDate")
    private String withdrawFinishDate;
}
