/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-22 09:22 创建
 */
package com.acooly.module.openapi.client.provider.yibao.domain;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 富滇银行存管基础报文
 *
 * @author zhangpu 2017-09-22 09:22
 */
public class YibaoMessage implements ApiMessage {

    /**
     * 采用RESTLET的URL路径作为服务唯一标志（服务名）
     */
    @NotEmpty
    @Length(max = 128)
    private String service;

    /**
     * 接入商户标志（商户号:merchantNo）
     */
    @NotEmpty
    @YibaoAlias(value = "merchantno")
    private String partner;

    public void doCheck() {

    }

    @Override
    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    @Override
    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
