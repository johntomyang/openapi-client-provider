package com.acooly.module.openapi.client.provider.yibao.exception;

/**
 * @author zhike 2018/3/15 14:28
 */
public class YibaoProcessingException extends RuntimeException{

    public YibaoProcessingException() {
    }

    public YibaoProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public YibaoProcessingException(String message) {
        super(message);
    }

    public YibaoProcessingException(Throwable cause) {
        super(cause);
    }
}
