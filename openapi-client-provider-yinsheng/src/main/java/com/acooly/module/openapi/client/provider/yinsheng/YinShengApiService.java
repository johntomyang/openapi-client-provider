/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.yinsheng;

import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengNotify;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import com.acooly.module.openapi.client.provider.yinsheng.message.*;
import com.acooly.module.openapi.client.provider.yinsheng.message.netbank.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author zhangpu
 */
@Service
public class YinShengApiService {

    @Resource(name = "yinShengApiServiceClient")
    private YinShengApiServiceClient apiServiceClient;

    @Autowired
    private OpenAPIClientYinShengProperties openAPIClientYinShengProperties;

    /**
     * 网关跳转充值
     *
     * @param request
     * @return
     */
    public String netBankDeposit(YinShengEBankDepositRequest request) {
        request.setMethod(YinShengServiceEnum.NETBANK_DEPOSIT.getCode());
        request.setNotify_url(openAPIClientYinShengProperties.getDomain()+"/gateway/notify/ysNotify/" + YinShengServiceEnum.NETBANK_DEPOSIT.getKey());
        return apiServiceClient.redirectGet(request);
    }

    /**
     * 快捷申请
     * @param request
     * @return
     */
    public YinShengFastPayResponse fastPay(YinShengFastPayRequest request) {
        request.setMethod(YinShengServiceEnum.FAST_PAY.getCode());
        request.setNotify_url(openAPIClientYinShengProperties.getDomain()+"/gateway/notify/ysNotify/" + YinShengServiceEnum.FAST_PAY.getKey());
        return (YinShengFastPayResponse)apiServiceClient.execute(request);
    }

    /**
     * 快捷认证
     * @param request
     * @return
     */
    public YinShengFastPayAuthorizeResponse fastPayAuthorize(YinShengFastPayAuthorizeRequest request) {
        request.setMethod(YinShengServiceEnum.FAST_PAY_AUTHORIZE.getCode());
        return (YinShengFastPayAuthorizeResponse)apiServiceClient.execute(request);
    }

    /**
     * 扫码支付
     * @param request
     * @return
     */
    public YinShengScanPayResponse scanPay(YinShengScanPayRequest request) {
        request.setNotify_url(openAPIClientYinShengProperties.getDomain()+"/gateway/notify/ysNotify/" + YinShengServiceEnum.SCAN_PAY.getKey());
        request.setMethod(YinShengServiceEnum.SCAN_PAY.getCode());
        return (YinShengScanPayResponse)apiServiceClient.execute(request);
    }

    /**
     * 微信公众号支付
     * @param request
     * @return
     */
    public YinShengJsPayResponse jsPay(YinShengJsPayRequest request) {
        request.setNotify_url(openAPIClientYinShengProperties.getDomain()+"/gateway/notify/ysNotify/" + YinShengServiceEnum.WECHAT_JSPAY.getKey());
        request.setMethod(YinShengServiceEnum.WECHAT_JSPAY.getCode());
        return (YinShengJsPayResponse)apiServiceClient.execute(request);
    }

    /**
     * 微信支付宝app支付
     * @param request
     * @return
     */
    public YinShengAppPayResponse appPay(YinShengAppPayRequest request) {
        request.setNotify_url(openAPIClientYinShengProperties.getDomain()+"/gateway/notify/ysNotify/" + YinShengServiceEnum.APP_PAY.getKey());
        request.setMethod(YinShengServiceEnum.APP_PAY.getCode());
        return (YinShengAppPayResponse)apiServiceClient.execute(request);
    }
    /**
     * 交易订单查询
     * @param request
     * @return
     */
    public YinShengTradeOrderQueryResponse tradeOrderQuery(YinShengTradeOrderQueryRequest request) {
        request.setMethod(YinShengServiceEnum.TRADE_ORDER_QUEERY.getCode());
        return (YinShengTradeOrderQueryResponse)apiServiceClient.execute(request);
    }
    /**
     * 实时提现结算接口
     * @param request
     * @return
     */
    public YinShengWithdrawD0Response withdrawD0(YinShengWithdrawD0Request request) {
        request.setNotify_url(openAPIClientYinShengProperties.getDomain()+"/gateway/notify/ysNotify/" + YinShengServiceEnum.WITHDRAW_D0.getKey());
        request.setMethod(YinShengServiceEnum.WITHDRAW_D0.getCode());
        return (YinShengWithdrawD0Response)apiServiceClient.execute(request);
    }

    /**
     * 商户余额查询
     * @param request
     * @return
     */
    public YinShengMerchantBalanceQueryResponse merchantBalanceQuery(YinShengMerchantBalanceQueryRequest request) {
        request.setMethod(YinShengServiceEnum.MERCHANT_BALANCE_QUERY.getCode());
        return (YinShengMerchantBalanceQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 对账文件下载
     * @param request
     * @return
     */
    public YinShengbillDownloadResponse billDownload(YinShengbillDownloadRequest request) {
        request.setMethod(YinShengServiceEnum.BILL_DOWNLOAD.getCode());
        return (YinShengbillDownloadResponse)apiServiceClient.execute(request);
    }


    /**
     * 解析异步通知
     *
     * @param request
     * @param serviceKey
     * @return
     */
    public YinShengNotify notice(HttpServletRequest request, String serviceKey) {
        return  apiServiceClient.notice(request,serviceKey);
    }

    /**
     * 签名
     *
     * @param waitSignMap
     * @param partnerId
     * @return
     */
    public String sign(Map<String, String> waitSignMap, String partnerId) {
        return apiServiceClient.getSignMarshall().sign(waitSignMap, partnerId);
    }

    /**
     * 验签
     *
     * @param request
     * @return
     */
    public boolean verySign(HttpServletRequest request,String partnerId) {
        return apiServiceClient.getSignMarshall().verySign(request,partnerId);
    }


    /**
     * 验签
     *
     * @param responseData
     * @return
     */
    public boolean verySign(Map<String, String> responseData,String partnerId) {
        return apiServiceClient.getSignMarshall().verySign(responseData,partnerId);
    }
}
