package com.acooly.module.openapi.client.provider.yl.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yl.domain.YlApiMsgInfo;
import com.acooly.module.openapi.client.provider.yl.domain.YlRequest;
import com.acooly.module.openapi.client.provider.yl.enums.YlServiceEnum;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@YlApiMsgInfo(service = YlServiceEnum.YL_REAL_DEDUCT_QUERY, type = ApiMessageType.Request)
public class YlDeductQueryRequest extends YlRequest {

    /**
     * 要查询的交易流水
     */
    private String querySn;
    /**
     * 查询备注
     */
    private String queryRemark;
    /**
     * 0，批次未完成时，不返回明细 (默认值)   1，批次未完成时，返回所有明细
     */
    private String retType;
    /**
     * 交易明细号  如为空时查询整个报文明细，如不为空时查询指定明细SN流水
     */
    private String queryDetailSn;

}
