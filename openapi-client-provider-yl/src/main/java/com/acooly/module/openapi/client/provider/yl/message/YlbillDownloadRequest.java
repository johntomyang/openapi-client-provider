package com.acooly.module.openapi.client.provider.yl.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yl.domain.YlApiMsgInfo;
import com.acooly.module.openapi.client.provider.yl.domain.YlRequest;
import com.acooly.module.openapi.client.provider.yl.enums.YlServiceEnum;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@YlApiMsgInfo(service = YlServiceEnum.YL_BILL_DOWNLOAD, type = ApiMessageType.Request)
public class YlbillDownloadRequest extends YlRequest {

    /** 渠道类型 */
    private String channelType;
    /** 代收or代付  S or F*/
    private String dsfFlag;
    /** 场次 */
    private String settNo;

    /** 商户号 */
    @Size(max=32)
    private String partnerId;

    /** 接入商名称 */
    @Size(max=255)
    private String partnerName;

    /** 商户私钥 */
    @Size(max=1024)
    private String partnerKey;

    /** 终端号 */
    @Size(max=32)
    private String terminalNo;

    /** 其他账号 */
    @Size(max=32)
    private String otherNo;

    /** 私钥文件路径 */
    @Size(max=255)
    private String privateKeyFilePath;

    /** 私钥密码 */
    @Size(max=255)
    private String privateKeyPsw;

    /** 公钥文件路径 */
    @Size(max=255)
    private String publicKeyFilePath;

    /** 证书 */
    @Size(max=1024)
    private String certificate;

    /** 父商户号 */
    @Size(max=32)
    private String parentId;

    /** 访问地址 */
    @Size(max=255)
    private String visitUrl;

    private String filePath;

    private String periodNo;
}
