/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-03 13:25 创建
 */
package com.acooly.openapi.client.provider.fudian;

import com.acooly.test.NoWebTestBase;
import com.rd.bds.common.util.RdStringUtil;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * @author zhangpu 2018-02-03 13:25
 */
public class FudianAbstractTest extends NoWebTestBase {

    // copy from dudian demo for test


    protected String getOrgCode(){
        String OrgCode = "";
        String str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String code = getRandomNum(8);
        int[] ws = { 3, 7, 9, 10, 5, 8, 4, 2 };
        int sum = 0;
        for (int i = 0; i < 8; i++) {
            sum += str.indexOf(String.valueOf(code.charAt(i))) * ws[i];
        }
        int c9 = 11 - (sum % 11);

        String sc9 = String.valueOf(c9);
        if (11 == c9) {
            sc9 = "0";
        } else if (10 == c9) {
            sc9 = "X";
        }
        OrgCode = code + "-" + sc9;
        return OrgCode;
    }

    protected String getLicenceCode(){
        String licenceCode = "";
        String code = getRandomNum(14);
        int[] s = new int[20];
        int[] p = new int[20];
        int[] a = new int[20];
        int m = 10;
        p[0] = m;
        for (int i = 0; i < code.length(); i++) {
            a[i] = RdStringUtil.toInt(code.substring(i, i + 1));
            s[i] = (p[i] % (m + 1)) + a[i];
            if (s[i] % m == 0) {
                p[i + 1] = 10 * 2;
            } else {
                p[i + 1] = (s[i] % m) * 2;
            }
        }
        a[14] = 11 - p[14] % 11;
        licenceCode = code + a[14];
        return licenceCode;
    }

    protected String getTxtRegCode(){
        return getRandomNum(15);
    }

    protected String getCreditCode(){
        String baseCode = "0123456789ABCDEFGHJKLMNPQRTUWXY";
        String code = getRandomNum(17);
        int[] wi = { 1, 3, 9, 27, 19, 26, 16, 17, 20, 29, 25, 13, 8, 24, 10, 30, 28 };
        int sum = 0;
        for (int i = 0; i < 17; i++) {
            sum += baseCode.indexOf(String.valueOf(code.charAt(i))) * wi[i];
        }
        int c18 = 31 - sum % 31;
        return code + c18;
    }


    protected String getRandomCode(int length) {
        if (length >= 10) {
            throw new RuntimeException("长度必须是10以内");
        }
        StringBuffer buffer = new StringBuffer();
        Random random = new Random();
        Set<Integer> set = new HashSet<Integer>();
        while (set.size() < length) {
            int a = random.nextInt(10);
            if (set.add(a)) {
                buffer.append(a);
            }
        }
        return buffer.toString();
    }

    /**
     * 随机生成数字字符串
     */
    protected String getRandomNum(int length) {
        StringBuffer buffer = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            int a = random.nextInt(10);
            buffer.append(a);
        }
        return buffer.toString();
    }


}
