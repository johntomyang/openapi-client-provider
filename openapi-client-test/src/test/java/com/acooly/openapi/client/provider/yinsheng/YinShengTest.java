package com.acooly.openapi.client.provider.yinsheng;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.yinsheng.YinShengApiService;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengAppPayBankTypeEnum;
import com.acooly.module.openapi.client.provider.yinsheng.message.dto.YinShengAppPayInfo;
import com.acooly.module.openapi.client.provider.yinsheng.message.dto.YinShengJsPayInfo;
import com.acooly.module.openapi.client.provider.yinsheng.message.dto.YinShengScanPayInfo;
import com.acooly.module.openapi.client.provider.yinsheng.message.netbank.YinShengAppPayRequest;
import com.acooly.module.openapi.client.provider.yinsheng.message.netbank.YinShengAppPayResponse;
import com.acooly.module.openapi.client.provider.yinsheng.message.netbank.YinShengJsPayRequest;
import com.acooly.module.openapi.client.provider.yinsheng.message.netbank.YinShengJsPayResponse;
import com.acooly.module.openapi.client.provider.yinsheng.message.netbank.YinShengScanPayRequest;
import com.acooly.module.openapi.client.provider.yinsheng.message.netbank.YinShengScanPayResponse;
import com.acooly.module.openapi.client.provider.yinsheng.message.*;
import com.acooly.module.openapi.client.provider.yinsheng.message.dto.*;
import com.acooly.module.openapi.client.provider.yinsheng.message.netbank.*;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
@BootApp(sysName = "YinShengTest")
public class YinShengTest extends NoWebTestBase {
    @Autowired
    private YinShengApiService yinShengApiService;

    /**
     * 测试扫码支付
     */
    @Test
    public void testScanPay() {
        YinShengScanPayRequest request = new YinShengScanPayRequest();
        request.setMethod("ysepay.online.qrcodepay");
        request.setTimestamp(Dates.format(new Date(),Dates.CHINESE_DATETIME_FORMAT_LINE));
        YinShengScanPayInfo shengScanPayInfo = new YinShengScanPayInfo();
        shengScanPayInfo.setOut_trade_no(Ids.oid());
        shengScanPayInfo.setShopdate(Dates.format(new Date(),Dates.CHINESE_DATE_FORMAT_LINE));
        shengScanPayInfo.setSubject("测试支付");
        shengScanPayInfo.setTotal_amount("0.1");
        shengScanPayInfo.setCurrency("CNY");
        shengScanPayInfo.setSeller_id("shanghu_test");
        shengScanPayInfo.setSeller_name("银盛支付商户测试公司");
        shengScanPayInfo.setTimeout_express("3d");
        shengScanPayInfo.setBusiness_code("01000010");
        shengScanPayInfo.setBank_type("1902000");
        request.setYinShengScanPayInfo(shengScanPayInfo);
        YinShengScanPayResponse response = yinShengApiService.scanPay(request);
        System.out.println("扫码支付响应报文："+ JSON.toJSONString(response));
    }


    /**
     * 测试微信公众号支付
     */
    @Test
    public void testJsPay() {
        YinShengJsPayRequest request = new YinShengJsPayRequest();
        request.setTimestamp(Dates.format(new Date(),Dates.CHINESE_DATETIME_FORMAT_LINE));
        YinShengJsPayInfo info = new YinShengJsPayInfo();
        info.setOut_trade_no(Ids.oid());
        info.setShopdate(Dates.format(new Date(),Dates.CHINESE_DATE_FORMAT_LINE));
        info.setSubject("测试支付");
        info.setTotal_amount("0.1");
        info.setCurrency("CNY");
        info.setSeller_id("shanghu_test");
        info.setSeller_name("银盛支付商户测试公司");
        info.setTimeout_express("3d");
        info.setBusiness_code("01000010");
        info.setSub_openid("323423423412234234");
        request.setYinShengJsPayInfo(info);
        YinShengJsPayResponse response = yinShengApiService.jsPay(request);
        System.out.println("微信公众号支付响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试微信支付宝app支付
     */
    @Test
    public void testAppPay() {
        YinShengAppPayRequest request = new YinShengAppPayRequest();
        request.setTimestamp(Dates.format(new Date(),Dates.CHINESE_DATETIME_FORMAT_LINE));
        YinShengAppPayInfo info = new YinShengAppPayInfo();
        info.setOut_trade_no(Ids.oid());
        info.setShopdate(Dates.format(new Date(),Dates.CHINESE_DATE_FORMAT_LINE));
        info.setSubject("测试支付");
        info.setTotal_amount("0.1");
        info.setCurrency("CNY");
        info.setSeller_id("shanghu_test");
        info.setSeller_name("银盛支付商户测试公司");
        info.setTimeout_express("3d");
        info.setBusiness_code("01000010");
        info.setBank_type(YinShengAppPayBankTypeEnum.ALI_APP_PAY.getCode());
        info.setLimit_credit_pay("0");
        info.setAppid("3234234234234234");
        request.setYinShengAppPayInfo(info);
        YinShengAppPayResponse response = yinShengApiService.appPay(request);
        System.out.println("微信支付宝App支付响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试D0提现结算
     */
    @Test
    public void testWithdrawD0() {
        YinShengWithdrawD0Request request = new YinShengWithdrawD0Request();
        String dataStr = Dates.format(new Date(),"yyyyMMdd");
        request.setTimestamp(Dates.format(new Date(),Dates.CHINESE_DATETIME_FORMAT_LINE));
        request.setGatewayUrl("https://commonapi.ysepay.com/gateway.do");
        request.setPartner_id("zdyasp888");
        YinShengWithdrawD0Info yinShengWithdrawD0Info = new YinShengWithdrawD0Info();
        yinShengWithdrawD0Info.setOut_trade_no(dataStr+Ids.oid());
        yinShengWithdrawD0Info.setShopdate(dataStr);
        yinShengWithdrawD0Info.setSubject("任性提现");
        yinShengWithdrawD0Info.setTotal_amount("3");
        yinShengWithdrawD0Info.setMerchant_usercode("zdyasp888");
        request.setYinShengWithdrawD0Info(yinShengWithdrawD0Info);
        YinShengWithdrawD0Response response = yinShengApiService.withdrawD0(request);
        System.out.println("实时提现响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试实时提现
     */
    @Test
    public void testNetBank() {
        YinShengEBankDepositRequest request = new YinShengEBankDepositRequest();
        String dataStr = Dates.format(new Date(),"yyyyMMdd");
        request.setTimestamp(Dates.format(new Date(),Dates.CHINESE_DATETIME_FORMAT_LINE));
        request.setGatewayUrl("https://openapi.ysepay.com/gateway.do");
        request.setPartner_id("zdyasp888");
        request.setOut_trade_no(Ids.oid());
        request.setShopdate(dataStr);
        request.setSubject("任性充值");
        request.setTotal_amount("100");
        request.setSeller_name("中大（延安）商品交易中心有限公司");
        request.setSupport_card_type("debit");
        request.setBank_account_type("personal");
        request.setBank_type("3021000");
        request.setReturn_url("http://218.70.106.250:8881/gateway/redirect/netBank/ysNetBankRedirect.html");
        request.setBusiness_code("3010002");
        request.setSeller_id("zdyasp888");
        String response = yinShengApiService.netBankDeposit(request);
        System.out.println("网关充值响应报文："+ response);
    }

    /**
     * 测试商户余额查询接口
     */
    @Test
    public void merchantBalanceQuery() {
        YinShengMerchantBalanceQueryRequest request = new YinShengMerchantBalanceQueryRequest();
        request.setTimestamp(Dates.format(new Date(),Dates.CHINESE_DATETIME_FORMAT_LINE));
        request.setGatewayUrl("https://commonapi.ysepay.com/gateway.do");
        request.setPartner_id("zdyasp888");
        YinShengMerchantBalanceQueryInfo requestInfo = new YinShengMerchantBalanceQueryInfo();
        requestInfo.setMerchant_usercode("zdyasp888");
        request.setYinShengMerchantBalanceQueryInfo(requestInfo);
        YinShengMerchantBalanceQueryResponse response = yinShengApiService.merchantBalanceQuery(request);
        System.out.println("商户账户余额查询响应报文："+ JSON.toJSONString(response));
    }
}
